import React from 'react';
import renderer from 'react-test-renderer';
import { Error } from '../../../../src/client/components/Error';

describe('<Error/>', () => {
    test('should be rendered correctly', () => {
        const component = renderer.create(<Error />);
        const tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });
});

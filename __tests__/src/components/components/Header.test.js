import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGithub, faTelegram } from '@fortawesome/free-brands-svg-icons';
import { faEnvelope, faBars } from '@fortawesome/free-solid-svg-icons';
import renderer from 'react-test-renderer';
import { Header } from '../../../../src/client/components/Header';

describe('<Header/>', () => {
    test('should be rendered correctly', () => {
        const openSidebar = jest.fn();
        const component = renderer.create(<Header openSidebar={openSidebar} />);
        const tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });
});

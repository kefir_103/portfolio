import React from 'react';
import renderer from 'react-test-renderer';
import { Sidebar } from '../../../../src/client/components/Sidebar';
import { Router } from 'react-router';
import { history } from '../../../../src/client/history';

describe('<Sidebar/>', () => {
    test('should be rendered correctly with isSidebarOpen = true', () => {
        const openSidebar = jest.fn();
        const component = renderer.create(
            <Router history={history}>
                <Sidebar isSidebarOpen={true} openSidebar={openSidebar} />
            </Router>
        );
        const tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });
    test('should be rendered correctly with isSidebarOpen = false', () => {
        const openSidebar = jest.fn();
        const component = renderer.create(
            <Router history={history}>
                <Sidebar isSidebarOpen={false} openSidebar={openSidebar} />
            </Router>
        );
        const tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });
});

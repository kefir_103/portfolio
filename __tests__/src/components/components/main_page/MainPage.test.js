import React from 'react';
import renderer from 'react-test-renderer';
import { MainPage } from '../../../../../src/client/components/main_page/MainPage';

describe('<MainPage/>', () => {
    test('should render correctly', () => {
        const component = renderer.create(<MainPage />);
        const tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });
});

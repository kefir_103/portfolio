import React from 'react';
import { ProjectDescription } from '../../../../../src/client/components/projects/ProjectDescription';
import { PROJECTS } from '../../../../../src/client/components/projects/constProjects';
import renderer from 'react-test-renderer';

describe('<ProjectDescription/>', () => {
    test('should be rendered correctly with state', () => {
        const location = {
            state: [...PROJECTS],
        };
        const match = {
            params: {
                app: 'Github Dashboard',
            },
        };
        const component = renderer.create(
            <ProjectDescription location={location} match={match} />
        );
        const tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });
    test('should be rendered correctly without state', () => {
        const location = {};
        const match = {
            params: {
                app: 'Github Dashboard',
            },
        };
        const component = renderer.create(
            <ProjectDescription location={location} match={match} />
        );
        const tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });
});

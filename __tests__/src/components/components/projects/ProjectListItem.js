jest.mock('../../../../../src/client/components/projects/ProjectDescription');
jest.mock('react-clamp-lines', () => 'ClampLines');

import React from 'react';
import { ProjectsListItem } from '../../../../../src/client/components/projects/ProjectsListItem';
import { PROJECTS } from '../../../../../src/client/components/projects/constProjects';
import renderer from 'react-test-renderer';

describe('<ProjectsListItem/>', () => {
    test('should be rendered correctly', () => {
        const project = { ...PROJECTS[0] };
        const component = renderer.create(
            <ProjectsListItem project={project} />
        );
        const tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });
});

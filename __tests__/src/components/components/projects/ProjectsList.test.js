jest.mock('../../../../../src/client/components/projects/ProjectsListItem');

import React from 'react';
import renderer from 'react-test-renderer';
import { history } from '../../../../../src/client/history';
import { ProjectsList } from '../../../../../src/client/components/projects/ProjectsList';
import { PROJECTS } from '../../../../../src/client/components/projects/constProjects';
import { Router } from 'react-router';

describe('<ProjectsList/>', () => {
    test('should be rendered correctly', () => {
        const component = renderer.create(
            <ProjectsList {...history} projects={PROJECTS} />
        );
        const tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });
});

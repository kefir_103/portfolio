module.exports = {
    bracketSpacing: true,
    jsxBracketSameLine: true,
    jsxSingleQuote: true,
    printWidth: 80,
    semi: true,
    singleQuote: true,
    tabWidth: 4,
    plugins: ['prettier-plugin-packagejson'],
};

import React from 'react';
import { Route, Switch } from 'react-router';
import { Router } from 'react-router-dom';
import { MainPage } from './components/main_page/MainPage';
import { Header } from './components/Header';
import { history } from './history';
import { Sidebar } from './components/Sidebar';
import { ProjectsList } from './components/projects/ProjectsList';
import { Footer } from './components/Footer';
import { ProjectDescription } from './components/projects/ProjectDescription';
import { PROJECTS } from './components/projects/constProjects';
import { Error } from './components/Error';

export class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isSidebarOpen: false,
        };

        this.openSidebar = this.openSidebar.bind(this);
    }

    openSidebar() {
        this.setState({
            isSidebarOpen: !this.state.isSidebarOpen,
        });
    }

    render() {
        return (
            <Router history={history}>
                <Header openSidebar={this.openSidebar} />
                <Sidebar
                    isSidebarOpen={this.state.isSidebarOpen}
                    openSidebar={this.openSidebar}
                />
                <Switch>
                    <Route
                        exact
                        path={'/'}
                        render={(props) => (
                            <MainPage
                                {...props}
                                isSidebarOpen={this.state.isSidebarOpen}
                            />
                        )}
                    />
                    <Route
                        exact
                        path={'/projects'}
                        render={(props) => (
                            <ProjectsList {...props} projects={PROJECTS} />
                        )}
                    />
                    <Route
                        path={'/projects/:app'}
                        component={ProjectDescription}
                    />
                    <Route path={'*'} component={Error} />
                </Switch>
                <Footer />
            </Router>
        );
    }
}

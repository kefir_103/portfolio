import React from 'react';

export const Error = () => {
    return (
        <div className={'app-container error'}>
            <h1>Упс :(</h1>
            <h2>Произошла ошибочка...</h2>
        </div>
    );
};

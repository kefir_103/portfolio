import React from 'react';

export const Footer = () => {
    return (
        <footer>
            <p>
                Пушкарев Ефим Владимирович{' '}
                <a href={'mailto:pushkarevev@yandex.ru'}>
                    pushkarevev@yandex.ru
                </a>
            </p>
        </footer>
    );
};

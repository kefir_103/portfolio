import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGithub, faTelegram } from '@fortawesome/free-brands-svg-icons';
import { faEnvelope, faBars } from '@fortawesome/free-solid-svg-icons';

export const Header = (props) => {
    return (
        <header>
            <FontAwesomeIcon
                id={'sidebar-opener'}
                icon={faBars}
                size={'2x'}
                onClick={props.openSidebar}
            />
            <h1>Пушкарев Ефим Владимирович</h1>
            <a href={'mailto:pushkarevev@yandex.ru'} target={'_blank'}>
                <FontAwesomeIcon
                    icon={faEnvelope}
                    size={'2x'}
                    color={'whitesmoke'}
                />
            </a>
            <a href='https://github.com/Kefir103' target={'_blank'}>
                <FontAwesomeIcon
                    icon={faGithub}
                    size={'2x'}
                    color={'whitesmoke'}
                />
            </a>
            <a href={'https://t.me/kefir_103'} target={'_blank'}>
                <FontAwesomeIcon
                    icon={faTelegram}
                    size={'2x'}
                    color={'whitesmoke'}
                />
            </a>
        </header>
    );
};

import React from 'react';
import { NavLink } from 'react-router-dom';

export const Sidebar = (props) => {
    return (
        <aside>
            <nav
                style={
                    props.isSidebarOpen
                        ? { left: '0', opacity: '1' }
                        : { left: '-250px', opacity: '0' }
                }>
                <NavLink exact to={'/'} activeClassName={'current-page'}>
                    Главная страница
                </NavLink>
                <NavLink to={'/projects'} activeClassName={'current-page'}>
                    Мои проекты
                </NavLink>
            </nav>
            <div
                id={'sidebar-additional'}
                style={
                    props.isSidebarOpen
                        ? {
                              left: '250px',
                              width: '100%',
                              opacity: '0.6',
                          }
                        : {
                              left: 0,
                              width: 0,
                              opacity: 0,
                          }
                }
                onClick={props.openSidebar}
            />
        </aside>
    );
};

import React from 'react';
import MyPhoto from '../../../../public/pictures/my_photo.jpeg';

export const MainPage = () => {
    return (
        <div className={'app-container main-page'}>
            <h1>Пушкарев Ефим Владимирович</h1>
            <h3>23 года, г. Москва</h3>
            <h3>Junior Frontend Developer</h3>
            <div className={'info'}>
                <img src={MyPhoto} alt={'Мое Фото'} />
                <ul>
                    <p style={{ textAlign: 'center' }}>
                        Несколько фактов обо мне:
                    </p>
                    <li>
                        Окончил бакалавриат в РТУ МИРЭА (2015-2019гг.) по
                        специальности 09.03.01 Информатика и вычислительная
                        техника (кафедра Вычислительной Техники, профиль:
                        «Вычислительные машины, комплексы, системы и сети»)
                    </li>
                    <li>
                        Стал увлекаться миром IT еще будучи школьником, и с тех
                        пор любовь к данной сфере не угасает
                    </li>
                    <li>
                        Увлечение непосредственно разработкой клиентской части
                        приложения началось в университете с мобильной
                        разработки под ОС Android (Java)
                    </li>
                    <li>
                        На текущий момент почти полностью перешел на Front-end
                        Web программирование, но интерес к мобильной разработке
                        никуда не пропал
                    </li>
                    <li>
                        В качестве любимого инструмента для Front-end разработки
                        выступает <a href={'https://ru.reactjs.org/'}>React</a>,
                        но также имеется опыт использования{' '}
                        <a href={'https://angular.io/'}>Angular</a> и{' '}
                        <a href={'https://reactnative.dev'}>React Native</a>
                    </li>
                    <li>
                        В качестве остальных инструментов люблю использовать:{' '}
                        <ul>
                            <li>
                                Backend:{' '}
                                <a href={'https://nodejs.org/en/'}>NodeJS</a> +{' '}
                                <a href={'https://expressjs.com/ru/'}>
                                    Express
                                </a>
                            </li>
                            <li>
                                Тестирование:{' '}
                                <a href={'https://jestjs.io/'}>Jest</a>
                            </li>
                            <li>
                                State Management:{' '}
                                <a href={'https://redux.js.org/'}>Redux</a>
                            </li>
                            <li>
                                CSS Pre-processor:{' '}
                                <a href={'https://sass-scss.ru/'}>SCSS</a>
                            </li>
                            <li>
                                Code formatting:{' '}
                                <a href={'https://prettier.io/'}>Prettier</a> +{' '}
                                <a href={'https://eslint.org/'}>ESLint</a>
                            </li>
                            <li>
                                Routing:{' '}
                                <a href={'https://reactrouter.com/'}>
                                    React Router
                                </a>
                            </li>
                            <li>
                                Building:{' '}
                                <a href={'https://babeljs.io/'}>Babel</a> +{' '}
                                <a href={'https://webpack.js.org/'}>Webpack</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        Также имеется опыт использования:{' '}
                        <a href={'https://www.typescriptlang.org/'}>
                            TypeScript
                        </a>
                        , <a href={'https://ejs.co/'}>EJS</a>,{' '}
                        <a href={'https://www.npmjs.com/package/jimp'}>Jimp</a>,{' '}
                        <a href={'https://www.npmjs.com/package/axios'}>
                            Axios
                        </a>
                        ,{' '}
                        <a href={'https://enzymejs.github.io/enzyme/'}>
                            Enzyme
                        </a>
                        ,{' '}
                        <a href={'https://github.com/reduxjs/redux-mock-store'}>
                            redux-mock-store
                        </a>
                        ,{' '}
                        <a
                            href={
                                'https://www.npmjs.com/package/fetch-mock-jest'
                            }>
                            fetch-mock-jest
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    );
};

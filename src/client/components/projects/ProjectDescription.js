import React from 'react';
import { PROJECTS } from './constProjects';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

export class ProjectDescription extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let project;
        if (this.props.location.state) {
            project = { ...this.props.location.state[0] };
        } else {
            project = PROJECTS.find(
                ({ name }) => name === this.props.match.params.app
            );
        }

        return (
            <div className={'app-container'}>
                <div
                    className={'pushback-button'}
                    onClick={() => this.props.history.goBack()}>
                    <FontAwesomeIcon icon={faArrowLeft} size={'1x'} />
                    Назад
                </div>
                <h2>{project.name}</h2>
                <p>{project.description}</p>
                <span>
                    <strong>Технические условия</strong>
                </span>
                <ol>
                    {project.technicalSpecifications.map((task, index) => (
                        <li key={'pdSpecsLi' + index}>{task}</li>
                    ))}
                </ol>
                <span>
                    <strong>Используемые инструменты</strong>
                </span>
                <ul>
                    {project.tools.map((tool, index) => (
                        <li key={'pdToolsLi' + index}>
                            {tool.usage}:{' '}
                            {tool.toolkit.map((toolkit, index) => {
                                if (index !== tool.toolkit.length - 1) {
                                    return [
                                        <a
                                            href={toolkit.url}
                                            key={'pdToolsLiHref' + index}>
                                            {toolkit.name}
                                        </a>,
                                        ' + ',
                                    ];
                                }
                                return (
                                    <a
                                        href={toolkit.url}
                                        key={'pdToolsLiHref' + index}>
                                        {toolkit.name}
                                    </a>
                                );
                            })}
                        </li>
                    ))}
                </ul>
                <div className={'project__links'}>
                    <a
                        href={project.link}
                        className={'project-link'}
                        target={'_blank'}>
                        Перейти на страницу проекта
                    </a>
                    {project.externalLinks.map((externalLink) => (
                        <a
                            href={externalLink.link}
                            className={'project-link'}
                            target={'_blank'}>
                            Ссылка на {externalLink.resourceName}
                        </a>
                    ))}
                </div>
            </div>
        );
    }
}

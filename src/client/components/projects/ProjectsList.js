import React from 'react';
import { ProjectsListItem } from './ProjectsListItem';

export const ProjectsList = (props) => {
    return (
        <div className={'app-container projects-list'}>
            <h3>Проекты, над которыми работал</h3>
            {props.projects.map((project, index) => (
                <ProjectsListItem
                    project={project}
                    history={props.history}
                    key={'ProjectsListItem' + index}
                />
            ))}
        </div>
    );
};

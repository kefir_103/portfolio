import React from 'react';
import ClampLines from 'react-clamp-lines/lib';

export class ProjectsListItem extends React.Component {
    constructor(props) {
        super(props);

        this.openProject = this.openProject.bind(this);
    }

    openProject(projectName) {
        this.props.history.push(`/projects/${projectName}`, [
            this.props.project,
        ]);
    }

    render() {
        return (
            <div
                className={'projects-list-item'}
                onClick={() => this.openProject(this.props.project.name)}>
                <p style={{ textAlign: 'center' }}>
                    <strong>{this.props.project.name}</strong>
                </p>
                <hr />
                <ClampLines
                    text={this.props.project.description}
                    lines={15}
                    ellipsis={'...'}
                    className={'project-list-description'}
                    buttons={false}
                />
            </div>
        );
    }
}

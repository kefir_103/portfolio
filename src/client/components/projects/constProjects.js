export const PROJECTS = [
    {
        name: 'Github Dashboard',
        description:
            'Данный проект использует Github API для создания веб-приложения, позволяющего вести поиск по репозиториям Github. ' +
            'Целью проекта является поиск наиболее популярных репозиториев' +
            ' из Github и получение информации по выбранному репозиторию',
        technicalSpecifications: [
            'Добавить поле поиска по репозиториям. В случае если поле поиска пустое, то выводить репозитории с наибольшим кол-вом звезд',
            'В элементе списка репозиториев выводить: Название репозитория, ссылку на репозиторий, кол-во звезд и дату последнего коммита',
            'На странице репозитория выводить: Название репозитория, кол-во звезд, ссылку на репозиторий,' +
                ' имя автора, аватар автора (если имеется), используемый язык, краткое описание репозитория и' +
                ' 10 наиболее активных контрибьютера',
            'Добавить элемент, позволяющий переходить по страницам (от 1 до 10 страницы)',
            'При перезагрузке страницы, используемые фильтры (поле поиска и номер страницы) должны сохраняться',
            'Дизайн веб-приложения должен быть адаптивным',
        ],
        tools: [
            {
                usage: 'Frontend',
                toolkit: [{ name: 'React', url: 'https://ru.reactjs.org/' }],
            },
            {
                usage: 'CSS Pre-processor',
                toolkit: [{ name: 'SCSS', url: 'https://sass-scss.ru/' }],
            },
            {
                usage: 'State Management',
                toolkit: [
                    { name: 'Redux', url: 'https://redux.js.org/' },
                    {
                        name: 'Redux-Thunk',
                        url: 'https://github.com/reduxjs/redux-thunk',
                    },
                ],
            },
            {
                usage: 'Testing',
                toolkit: [
                    { name: 'Jest', url: 'https://jestjs.io/' },
                    {
                        name: 'Enzyme',
                        url: 'https://enzymejs.github.io/enzyme/',
                    },
                    {
                        name: 'redux-mock-store',
                        url: 'https://github.com/reduxjs/redux-mock-store',
                    },
                    {
                        name: 'fetch-mock-jest',
                        url: 'https://www.npmjs.com/package/fetch-mock-jest',
                    },
                ],
            },
            {
                usage: 'Backend',
                toolkit: [
                    { name: 'NodeJS', url: 'https://nodejs.org/en/' },
                    { name: 'Express', url: 'https://expressjs.com/ru/' },
                ],
            },
            {
                usage: 'API',
                toolkit: [
                    {
                        name: 'Github API',
                        url: 'https://developer.github.com/v3/',
                    },
                ],
            },
            {
                usage: 'Requesting',
                toolkit: [
                    {
                        name: 'Fetch',
                        url:
                            'https://developer.mozilla.org/ru/docs/Web/API/Fetch_API',
                    },
                ],
            },
            {
                usage: 'Routing',
                toolkit: [
                    {
                        name: 'React Router',
                        url: 'https://reactrouter.com/',
                    },
                ],
            },
            {
                usage: 'Building',
                toolkit: [
                    {
                        name: 'Babel',
                        url: 'https://babeljs.io/',
                    },
                    {
                        name: 'Webpack',
                        url: 'https://webpack.js.org/',
                    },
                ],
            },
            {
                usage: 'Formatting',
                toolkit: [
                    {
                        name: 'Prettier',
                        url: 'https://prettier.io/',
                    },
                ],
            },
        ],
        link: 'https://pushkarev-github-dashboard.herokuapp.com/',
        externalLinks: [],
    },
    {
        name: 'WDS PixelPerfect',
        description:
            'Данный проект является многостраничным сайтом, верстка которого производилась' +
            ' по PSD макету с применением плагина PixelPerfect. Хочу обратить Ваше внимание на то,' +
            ' что на текущий момент данный сайт имеет верстку только под компьютеры и ноутбуки и не является адаптивным!',
        technicalSpecifications: [
            'Создать многостраничный сайт, верстка которого производилась по PSD макету',
            'Использовать технику PixelPerfect для попиксельного соответствия макету (с незначительными отклонениями по пикселям)',
            'В качестве методологии разработки веб-сайтов использовать БЭМ',
        ],
        tools: [
            {
                usage: 'Frontend',
                toolkit: [{ name: 'React', url: 'https://ru.reactjs.org/' }],
            },
            {
                usage: 'CSS Pre-processor',
                toolkit: [{ name: 'SCSS', url: 'https://sass-scss.ru/' }],
            },
            {
                usage: 'Backend',
                toolkit: [
                    { name: 'NodeJS', url: 'https://nodejs.org/en/' },
                    { name: 'Express', url: 'https://expressjs.com/ru/' },
                ],
            },
            {
                usage: 'Routing',
                toolkit: [
                    {
                        name: 'React Router',
                        url: 'https://reactrouter.com/',
                    },
                ],
            },
            {
                usage: 'Building',
                toolkit: [
                    {
                        name: 'Babel',
                        url: 'https://babeljs.io/',
                    },
                    {
                        name: 'Webpack',
                        url: 'https://webpack.js.org/',
                    },
                ],
            },
            {
                usage: 'Formatting',
                toolkit: [
                    {
                        name: 'Prettier',
                        url: 'https://prettier.io/',
                    },
                ],
            },
            {
                usage: 'Methodology',
                toolkit: [
                    {
                        name: 'БЭМ',
                        url: 'https://ru.bem.info/',
                    },
                ],
            },
        ],
        link: 'https://pushkarev-wds-pixelperfect.herokuapp.com/',
        externalLinks: [
            {
                link: 'https://github.com/Kefir103/wds_pixelperfect',
                resourceName: 'Github',
            },
            {
                link: 'http://www.pcklab.com/templates/modern-corporate-design',
                resourceName: 'PSD',
            },
        ],
    },
    {
        name: 'Контактный справочник',
        description:
            'В данном проекте реализован аналог контактного справочника, в котором пользователь может загрузить данные ' +
            'с сервера, отфильтровать и отсортировать данные, а также добавить или изменить их.',
        technicalSpecifications: [
            'Добавить возможность загружать данные с сервера и заносить их в таблицу приложения',
            'При нажатии на заголовки колонок должна происходить сортировка данных по выбранному столбцу таблицы',
            'Добавить фильтрацию данных по полям',
            'Сделать клиентскую пагинацию (50 элементов на страницу) в приложении',
            'При нажатии на определенный элемент таблицы, данные об этом элементе должны отображаться под таблицей с возможностью' +
                'изменять описание элемента',
            'Сделать форму для добавления нового элемента (добавление происходит в начало таблицы) с валидацией введенного в поле значения',
            'Обработка данных (загрузка, фильтрация, сортировка, изменение, добавление) должны происходить на клиенте приложения',
        ],
        tools: [
            {
                usage: 'Frontend',
                toolkit: [{ name: 'React', url: 'https://ru.reactjs.org/' }],
            },
            {
                usage: 'CSS Pre-processor',
                toolkit: [{ name: 'SCSS', url: 'https://sass-scss.ru/' }],
            },
            {
                usage: 'State Management',
                toolkit: [
                    { name: 'Redux', url: 'https://redux.js.org/' },
                    {
                        name: 'Redux-Thunk',
                        url: 'https://github.com/reduxjs/redux-thunk',
                    },
                ],
            },
            {
                usage: 'Testing',
                toolkit: [
                    { name: 'Jest', url: 'https://jestjs.io/' },
                    {
                        name: 'Enzyme',
                        url: 'https://enzymejs.github.io/enzyme/',
                    },
                    {
                        name: 'redux-mock-store',
                        url: 'https://github.com/reduxjs/redux-mock-store',
                    },
                    {
                        name: 'fetch-mock-jest',
                        url: 'https://www.npmjs.com/package/fetch-mock-jest',
                    },
                ],
            },
            {
                usage: 'Requesting',
                toolkit: [
                    {
                        name: 'Fetch',
                        url:
                            'https://developer.mozilla.org/ru/docs/Web/API/Fetch_API',
                    },
                ],
            },
            {
                usage: 'API',
                toolkit: [
                    {
                        name: 'Big data',
                        url:
                            'http://www.filltext.com/?rows=1000&id={number|1000}&firstName={firstName}&delay=3&lastName={lastName}&email={email}&phone={phone|(xxx)xxx-xx-xx}&address={addressObject}&description={lorem|32}',
                    },
                    {
                        name: 'Small data',
                        url:
                            'http://www.filltext.com/?rows=32&id={number|1000}&firstName={firstName}&lastName={lastName}&email={email}&phone={phone|(xxx)xxx-xx-xx}&address={addressObject}&description={lorem|32}',
                    },
                ],
            },
            {
                usage: 'Backend',
                toolkit: [
                    { name: 'NodeJS', url: 'https://nodejs.org/en/' },
                    { name: 'Express', url: 'https://expressjs.com/ru/' },
                ],
            },
            {
                usage: 'Building',
                toolkit: [
                    {
                        name: 'Babel',
                        url: 'https://babeljs.io/',
                    },
                    {
                        name: 'Webpack',
                        url: 'https://webpack.js.org/',
                    },
                ],
            },
            {
                usage: 'Formatting',
                toolkit: [
                    {
                        name: 'Prettier',
                        url: 'https://prettier.io/',
                    },
                ],
            },
        ],
        link: 'https://pushkarev-contact-directory.herokuapp.com/',
        externalLinks: [
            {
                link: 'https://github.com/Kefir103/contact-directory',
                resourceName: 'Github',
            },
        ],
    },
];
